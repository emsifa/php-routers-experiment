<?php namespace Rakit\Framework\Router;

use Closure;
use Rakit\Framework\App;
use Rakit\Framework\MacroableTrait;

class Router {

    /**
     * Kumpulan route yang di daftarkan
     * @var array
     */
    protected $routes = [];

    /**
     * List registered groups
     * @var array
     */
    protected $groups = array();

    /**
     * Max parameter pada sebuah route
     * digunakan juga untuk acuan dummy group pada method toRegex()
     * @var int
     */
    protected $max_params = 5;
    
    /**
     * Banyak route maksimum di dalam sebuah regex
     * @var int
     */
    protected $max_routes_in_regex = 16;

    /**
     * Default route parameter regex
     * @var string
     */
    protected $default_param_regex = '[^/]+';

    /**
     * Case sensitive route
     * @var boolean
     */
    protected $case_sensitive = true;

    /**
     * Get registered routes
     *
     * @return  array
     */
    public function getRoutes()
    {
        $routes = $this->routes;
        foreach($this->groups as $group) {
            $routes = array_merge($routes, $group->getRoutes());
        }

        return $routes;
    }

    /**
     * Register a Route
     *
     * @param   string $method
     * @param   string $path
     * @return  Route
     */
    public function add($method, $path, $handler)
    {
        $route = new Route($method, $path, $handler);
        $this->routes[] = $route;

        return $route;
    }

    /**
     * Grouping routes with Closure
     *
     * @param   string $path
     * @param   Closure $grouper
     * @return  RouteMap
     */
    public function group($path, Closure $grouper)
    {
        $group = new RouteGroup($path, $grouper);
        $this->groups[] = $group;
        return $group;
    }

    /**
     * Find route by given path and method
     *
     * @param   string $path
     * @param   string $method
     * @return  null|Route
     */
    public function dispatch($method, $path)
    {
        $pattern = $this->makePattern($method, $path);
        $all_routes = $this->getRoutes();

        $chunk_routes = array_chunk($all_routes, $this->max_routes_in_regex);

        foreach($chunk_routes as $i => $routes) {
            $regex = [];
            foreach($routes as $i => $route) {
                $regex[] = $this->toRegex($route, $i);
            }
            $regex = "~^(?|".implode("|", $regex).")$~x";

            if(!preg_match($regex, $pattern, $matches)) {
                continue;
            }

            $index = (count($matches) - 1 - $this->max_params);

            $matched_route = $routes[$index];

            $params = $this->getDeclaredPathParams($matched_route);
            foreach($params as $i => $param) {
                $matched_route->params[$param] = $matches[$i+1];
            }
            
            return $matched_route;
        }

        return null;
    }

    /**
     * Tranfrom route path into Regex
     *
     * @param   Route $route
     * @return  string regex
     */
    protected function toRegex(Route $route, $index)
    {
        $method = $route->getMethod();
        $path = $route->getPath();
        $conditions = $route->getConditions();
        $params = $this->getDeclaredPathParams($route);

        $regex = $this->makePattern($method, $path);

        foreach($params as $param) {
            if (array_key_exists($param, $conditions)) {
                $regex = str_replace(':'.$param, '('.$conditions[$param].')', $regex);
            } else {
                $regex = str_replace(':'.$param, '('.$this->default_param_regex.')', $regex);
            }
        }

        $count_brackets = substr_count($regex, "(");
        $count_added_brackets = $this->max_params + ($index - $count_brackets);

        $regex .= str_repeat("()", $count_added_brackets);

        return $regex;
    }

    /**
     * Getting declared parameters by given route object
     *
     * @param   Route $route
     * @return  array
     */
    protected function getDeclaredPathParams(Route $route)
    {
        $path = $route->getPath();
        preg_match_all('/\:([a-z_][a-z0-9_]+)/i', $path, $match);
        return $match[1];
    }

    public function makePattern($method, $path)
    {
        return $method.$path;
    }

}

Benchmark Kandidat Router PHP
================================

Repo ini berisi kandidat router dan benchmarkingnya. 
Nantinya router yang terbaik akan digunakan untuk [Rakit Framework](https://github.com/rakit/framework).

Dalam repo ini terdapat 2 buah benchmarking dan 2 buah kandidat Router.

## Kandidat Router (RouterA dan RouterB)

Kedua route ini mengadaptasi konsep yang sama dengan FastRoute, yaitu mengelompokan puluhan regex pada route menjadi sebuah regex yang cukup besar. Jadi Router tidak mencocokkan regex pada route 1/1 melainkan puluhan regex sekaligus. 

Perbedaan `RouterA.php` dan `RouterB.php` adalah pada proses membentuk regexnya. Jika pada `RouterA` regex dibentuk pada saat mendaftarkan route, pada `RouterB` regex dibentuk saat melakukan dispatch.  

> RouterA dan RouterB bersifat mentahan (belum matang), hanya untuk mengecek cara mana yang lebih cepat, Router yang dirasa lebih unggul akan di adaptasi ke file `Router/Router.php` untuk nantinya dimasukkan ke repo rakit framework.

## Metode Benchmarking

#### benchmark-1.php

Benchmark 1 ini di adaptasi dari [benchmark FastRoute](https://gist.github.com/nikic/9049180) yang ditulis di [artikel ini](http://nikic.github.io/2014/02/18/Fast-request-routing-using-regular-expressions.html). Pada benchmark ini performa dihitung saat route melakukan ribuan dispatching. 

#### benchmark-2.php

Menurut saya terdapat suatu kesalahan pada `benchmark-1`, karena dalam aplikasi nyatanya, 
umumnya Router hanya melakukan 1 kali dispatching untuk setiap request ke aplikasi. 

Itu artinya performa router seharusnya dihitung sejak awal inisialisasi, mendaftarkan routes, sampai akhirnya melakukan dispatching. Karena didalam router, terdapat pula proses yang cukup memakan waktu, yaitu proses mengubah path yang didaftarkan menjadi sebuah regex.

## Hasil (sementara)

Berikut ini hasil sementara di laptop celeron saya:

#### [benchmark-1] RouterA vs RouterB vs FastRoute

![Hasil benchmark-1](https://dl.dropboxusercontent.com/u/102070675/benchmark-router/benchmark-1.png)


#### [benchmark-2] RouterA vs RouterB vs FastRoute

![Hasil benchmark-2](https://dl.dropboxusercontent.com/u/102070675/benchmark-router/benchmark-2.png)

#### [benchmark-2] Router Rakit Framework (baru 90% jadi) vs FastRoute

![Hasil benchmark-2 FastRoute vs Router Rakit Framework](https://dl.dropboxusercontent.com/u/102070675/benchmark-router/benchmark-2b.png)

#### Kesimpulan (bisa berubah)

Pada `benchmark-1`, terlihat `RouterB` kalah jauh. Itu dikarenakan pada `benchmark-1`, performa dihitung pada saat melakukan ribuan dispatching, sedangkan `RouterB` proses dibebankan penuh pada metode `dispatch`nya. 

Selanjutnya bisa dilihat pada `benchmark-2`, ternyata `RouterB` lah yang paling unggul. Jadi `RouterB` ini saya adaptasi ke `Rakit\Framework\Router\Router`, dan hasilnya pada hasil ke-3, terlihat kalau Router (sementara) Rakit Framework lebih unggul dibanding `FastRoute`.


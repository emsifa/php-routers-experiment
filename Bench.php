<?php 

class Bench {

    protected static $marks = [];

    public static function mark($label)
    {
        static::$marks[$label] = microtime(true);
    }

    public static function show($label)
    {
        $now = microtime(true);
        if(!array_key_exists($label, static::$marks)) {
            print("[bench] label {$label} not found!\n");
        } else {
            print("[bench] {$label} => ".($now - static::$marks[$label])."ms\n");
        }
    }

}
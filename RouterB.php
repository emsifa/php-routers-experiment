<?php 


class RouterB {

    /**
     * Kumpulan route yang di daftarkan
     * @var array
     */
    protected $routes = [];

    /**
     * Max parameter pada sebuah route
     * digunakan juga untuk acuan dummy group pada method toRegex()
     * @var int
     */
    protected $max_params = 10;
    
    /**
     * Banyak route maksimum di dalam sebuah regex
     * @var int
     */
    protected $max_routes_in_regex = 16;

    public function add($method, $path, $data)
    {
        $pattern = $method.$path; 
        $this->routes[$pattern] = $data;
    }

    public function dispatch($method, $path)
    {
        $pattern = $method.$path;
        $route_keys = array_keys($this->routes);
        $route_datas = array_values($this->routes);

        $chunk_paths = array_chunk(array_keys($this->routes), $this->max_routes_in_regex);
        $chunk_datas = array_chunk($this->routes, $this->max_routes_in_regex);

        foreach($chunk_paths as $i => $paths) {
            $datas = $chunk_datas[$i];
            
            $regex = "";
            foreach($paths as $i => $path) {
                $regex .= "|".$this->toRegex($path, $i);
            }
            $regex = "~^(?|".ltrim($regex,"|").")$~x";

            if(!preg_match($regex, $pattern, $matches)) {
                continue;
            }

            $index = (count($matches) - 1 - $this->max_params);
            
            return [
                'route' => $paths[$index],
                'data' => $datas[$index]
            ];
        }

        return null;
    }

    public function toRegex($route, $i)
    {
        $regex = preg_replace("/:[a-zA-Z0-9_]+/", "([a-zA-Z0-9-_]+)", $route);

        $count_brackets = substr_count($regex, "(");
        $count_added_brackets = $this->max_params + ($i - $count_brackets);

        $regex .= str_repeat("()", $count_added_brackets);

        return $regex;
    }

}
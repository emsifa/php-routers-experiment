<?php 

require(__DIR__.'/vendor/autoload.php');
require(__DIR__.'/Bench.php');
require(__DIR__.'/RouterA.php');
require(__DIR__.'/RouterB.php');
require(__DIR__.'/Router/Router.php');
require(__DIR__.'/Router/Route.php');

$nRoutes = 100;
$nMatches = 1000;

$routers = ['Rakit\Framework\Router\Router'];//, 'RouterA', 'RouterB'];

foreach($routers as $router_class) {
    print("\n# ".$router_class."\n");
    print("-----------------------------------------\n");

    Bench::mark('first route');
    for($i=0; $i<$nMatches; $i++) {
        $router = new $router_class;

        for ($x = 0, $str = 'a'; $x < $nRoutes; $x++, $str++) {
            $path = '/'.$str.'/:arg';
            $router->add('GET', $path, 'handler '.$path);
            $lastStr = $str;
        }

        $first = $router->dispatch('GET', '/a/foo');
        unset($router);
    }
    Bench::show('first route');

    Bench::mark('last route');
    for($i=0; $i<$nMatches; $i++) {
        $router = new $router_class;

        for ($x = 0, $str = 'a'; $x < $nRoutes; $x++, $str++) {
            $path = '/'.$str.'/:arg';
            $router->add('GET', $path, 'handler '.$path);
            $lastStr = $str;
        }


        $last = $router->dispatch('GET', '/'.$lastStr.'/foo');
        unset($router);
    }
    Bench::show('last route');

    Bench::mark('unknown route');
    for($i=0; $i<$nMatches; $i++) {
        $router = new $router_class;

        for ($x = 0, $str = 'a'; $x < $nRoutes; $x++, $str++) {
            $path = '/'.$str.'/:arg';
            $router->add('GET', $path, 'handler '.$path);
            $lastStr = $str;
        }

        $not_found = $router->dispatch('GET', '/not-found');
        unset($router);
    }
    Bench::show('unknown route');

    // var_dump(compact('first', 'last', 'not_found'));
}

/**
 * FastRoute
 */
print("\n# FastRoute\n");
print("-----------------------------------------\n");

$options = [];
$lastStr = "";

Bench::mark('first route');
for($i=0; $i<$nMatches; $i++) {
    $router = FastRoute\simpleDispatcher(function($router) use ($nRoutes, &$lastStr) {
        for ($x = 0, $str = 'a'; $x < $nRoutes; $x++, $str++) {
            $path = '/'.$str.'/{arg}';
            $router->addRoute('GET', $path, 'handler '.$path);
            $lastStr = $str;
        }
    }, $options);

    $first = $router->dispatch('GET', '/a/foo');

    unset($router);
}
Bench::show('first route');

Bench::mark('last route');
for($i=0; $i<$nMatches; $i++) {
    $router = FastRoute\simpleDispatcher(function($router) use ($nRoutes, &$lastStr) {
        for ($x = 0, $str = 'a'; $x < $nRoutes; $x++, $str++) {
            $path = '/'.$str.'/{arg}';
            $router->addRoute('GET', $path, 'handler '.$path);
            $lastStr = $str;
        }
    }, $options);

    $last = $router->dispatch('GET', '/'.$lastStr.'/foo');
    unset($router);
}
Bench::show('last route');

Bench::mark('unknown route');
for($i=0; $i<$nMatches; $i++) {
    $router = FastRoute\simpleDispatcher(function($router) use ($nRoutes, &$lastStr) {
        for ($x = 0, $str = 'a'; $x < $nRoutes; $x++, $str++) {
            $path = '/'.$str.'/{arg}';
            $router->addRoute('GET', $path, 'handler '.$path);
            $lastStr = $str;
        }
    }, $options);

    $not_found = $router->dispatch('GET', '/not-found');
    unset($router);
}
Bench::show('unknown route');

// var_dump(compact('first', 'last', 'not_found'));

<?php 

require("vendor/autoload.php");
require(__DIR__.'/Bench.php');
require(__DIR__.'/RouterA.php');
require(__DIR__.'/RouterB.php');

$nRoutes = 100;
$nMatches = 10000;
$router_candidates = ['RouterA', 'RouterB'];

foreach($router_candidates as $router_class) {
    print("\n# ".$router_class."\n");
    print("-----------------------------------------\n");

    $router = new $router_class;

    // mendaftarkan route
    for ($i = 0, $str = 'a'; $i < $nRoutes; $i++, $str++) {
        $path = '/'.$str.'/:arg';
        $router->add('GET', $path, 'handler '.$path);
        $lastStr = $str;
    }

    // benchmarking untuk dispatching route pertama
    Bench::mark('first route');
    for($i=0; $i<$nMatches; $i++) {
        $first = $router->dispatch('GET', '/a/foo');
    }
    Bench::show('first route');

    // benchmarking untuk dispatching route terakhir
    Bench::mark('last route');
    for($i=0; $i<$nMatches; $i++) {
        $last = $router->dispatch('GET', '/'.$lastStr.'/foo');
    }
    Bench::show('last route');

    // benchmarking untuk dispatching route yang tidak terdaftar
    Bench::mark('unknown route');
    for($i=0; $i<$nMatches; $i++) {
        $not_found = $router->dispatch('GET', '/not-found');
    }
    Bench::show('unknown route');
}

/**
 * FastRoute
 */
print("\n# FastRoute\n");
print("-----------------------------------------\n");

$options = [];
$lastStr = "";

// inisialisasi dan mendaftarkan route
$router = FastRoute\simpleDispatcher(function($router) use ($nRoutes, &$lastStr) {
    for ($x = 0, $str = 'a'; $x < $nRoutes; $x++, $str++) {
        $path = '/'.$str.'/{arg}';
        $router->addRoute('GET', $path, 'handler '.$path);
        $lastStr = $str;
    }
}, $options);

// benchmarking untuk dispatching route pertama
Bench::mark('first route');
for($i=0; $i<$nMatches; $i++) {
    $first = $router->dispatch('GET', '/a/foo');
}
Bench::show('first route');

// benchmarking untuk dispatching route terakhir
Bench::mark('last route');
for($i=0; $i<$nMatches; $i++) {
    $last = $router->dispatch('GET', '/'.$lastStr.'/foo');
}
Bench::show('last route');

// benchmarking untuk dispatching route yang tidak terdaftar
Bench::mark('unknown route');
for($i=0; $i<$nMatches; $i++) {
    $not_found = $router->dispatch('GET', '/not-found');
}
Bench::show('unknown route');

// var_dump(compact('first', 'last', 'not_found'));

<?php 

class RouterA {

    /**
     * Kumpulan route yang di daftarkan
     * @var array
     */
    protected $routes = [];

    /**
     * Kumpulan regex yang akan di dispatch
     * dimana setiap regex mewakili $max_routes_in_regex route
     * @var array
     */
    protected $regexes = [];

    /**
     * Max parameter pada sebuah route
     * digunakan juga untuk acuan dummy group pada method toRegex()
     * @var int
     */
    protected $max_params = 10;
    
    /**
     * Banyak route maksimum di dalam sebuah regex
     * @var int
     */
    protected $max_routes_in_regex = 16;

    public function add($method, $path, $data)
    {
        $pattern = $method.$path; 
        $this->routes[$pattern] = $data;

        $count_routes = count($this->routes);
        $index_regex = floor($count_routes/$this->max_routes_in_regex);
        
        if(!isset($this->regexes[$index_regex])) {
            $this->regexes[$index_regex] = "";
        }

        $this->regexes[$index_regex] .= "|".$this->toRegex($pattern, $count_routes - ($index_regex*$this->max_routes_in_regex));
    }

    public function dispatch($method, $path)
    {
        $pattern = $method.$path;
        $route_keys = array_keys($this->routes);
        $route_datas = array_values($this->routes);

        foreach($this->regexes as $i => $regex) {
            $regex = "~^(?|".ltrim($regex,"|").")$~x";

            if(!preg_match($regex, $pattern, $matches)) {
                continue;
            }

            $index = ($this->max_routes_in_regex*$i)+(count($matches) - 1 - $this->max_params)-1;

            return [
                'route' => $route_keys[$index], 
                'data' => $route_datas[$index],
                // 'matches' => $matches
            ];
        }
    }

    public function toRegex($route, $i)
    {
        $regex = preg_replace("/:[a-zA-Z0-9_]+/", "([a-zA-Z0-9-_]+)", $route);

        $count_brackets = substr_count($regex, "(");
        $count_added_brackets = $this->max_params + ($i - $count_brackets);

        $regex .= str_repeat("()", $count_added_brackets);

        return $regex;
    }

}
